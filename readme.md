# Example of vanilla Web Components
Use W3C-standards Web Components to enable development without frameworks (and slow compilation steps).

## Dependencies (development only)
Only node is nescessary below, the rest is "nice to have" optional stuff.

* [Node](https://nodejs.org/en/download/) to run the devserver-script
* EsLint, `npm, install eslint -g` , Global/Local install to lint the code: `eslint src`
* Prettier, Formats the code to keep a consistent codebase (`.vscode/extensions.json` contains recommended plugin for VS Code). 

## Get up and running
* Open terminal and run: `node devserver`
* Open http://127:0.0.1:2001 in a browser 
* *Perform changes in the codebase and the browser will reload*

## Production builds
Not included but if needed run a minifier and if older browser support needed then use babel or similar. Webpack is one possible solution if more advanced deploy is really needed.

## Topics for bootcamp
- History (css, jquery... the many frameworks once ES6 and Html5 showed up 2015: https://www.w3schools.com/js/js_versions.asp )
    * Different approaches: Vue -> React -> Angular
    * Today many framework use a deep toolchain: Ex: react, redux, react-router, babel, webpack, plugins (css modules, map-files), lots of npm-packages
        -> Every part require knowledge
        -> Constantly evolving
- When starting to learn it's not a good idea to mix to many tech-stacks at once
    ... like JS, TypeScript, React(JSX), Redux
    -> what is part of JS and what is TS and what is React... easy to get confused!
- Very effective to build stuff from scratch instead of start by introducing a dependency that...
    * Is written in a way that is hard to understand
    * Not maintained when update to framwork is updated
    * Is often generic and solves way more than is needed (more code == more bugs)
    * The many dependencies of npm-packages... ex: npx-create-app installes several hundred MB.
    * Most components on github have one maintainer that adds 99% of the code which is not optimal for longevity of your solution. 
    ** Instead you get an understanding of how things work... then you can use an abstraction in future projects!
- Html/CSS:
    * Script type "module"
    * CSS reset
    * Global scope
    * Use your favourite css, for example Bootstrap.. here we use: https://www.w3schools.com/w3css/default.asp
- Javascript:
    - import (import * as ...)
    - bind methods & variables to class context
    - Event-handlers
    - Fetch (see api.js)
- WebComponents, W3C-standard supported in all evergreen browsers:
    * For custom component extend `HtmlElement`
    * For specilization of existing elements use for example `extends HtmlDivElement` and then use with `<div is="mycustom-div`>`    
    * [Web Components](https://github.com/w3c/webcomponents)
        ** [Lifecycle callbacks](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements#using_the_lifecycle_callbacks)
    * [Custom Elements](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements), [browser-support](https://caniuse.com/#feat=custom-elementsv1)
    * [Shadow DOM](https://www.w3.org/TR/shadow-dom/) for encapsulation, [browser-support](https://caniuse.com/#feat=shadowdomv1)
    * [Custom CSS Properties](https://www.w3.org/TR/css-variables-1/) for global css variables, [browser-support](https://caniuse.com/#search=custom%20properties)
    * ES6 Javascript Import etc. (we may even use dynamic import in some cases).
    * Why Shadow-Dom? -> (isolate css/js/html and make updates faster -> enable components)
    * global css does not travel into Shadow Dom
    * Lifecycle methods
    * Slots
    * Pass props (observedAttributes, attributeChangedCallback).. Provide a static list of attr
- Local storage:
	* IndexedDB API for larger stuff: https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API
    * For minor stuff use: https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API
- Coding conventions/Tools:
    * Lint
    * Prettier    
    * jsdoc: https://jsdoc.app/
    * css-string formatting
	* html-string formatting
- Patterns:    
    * Immutability

## Source code 
* `devserver.js` : Runs a http-server in node and listens on port 2001. Will monitor filechanges on all files that has been loaded by the browser. The client will poll the http-server and if any files are changed will return a list of changed files. 
* `src/index.js` : Is loaded from index.html and is currently only used for development. Interacts with `devserver.js` using http and will reload the application if the server reports changed files (keeps a connection open as long as possible and then opens a new).
* `src/global.css` : Global styles that also contains a css-reset to ensure browser consistent layout.
* `src/index.html` : Default webpage that reference `src/components/App.js` and places an app-component on the page. It also loads the global stylesheet `global.css`.
* `src/components/App.js` : The main WebComponent that is responsible for application-level layout. Also catches unhandled application level errors.
* `src/components/CheckboxControl.js` : Example of a webcomponent that encapulates a standard html-checkbox. Remembers state between server loads.
* `src/components/LayoutControl.js` : Example of a simple layout control that can be nested.
* `src/api/api.js` : Api-functions get & post that wraps standard fetch in error handling.

## Tips & Tricks
* Get property value(named "propertyname" in this case) in child-component: `this.getAttribute("propertyname")`
* Notify property changes with attributeChangedCallback + observedAttributes 
* Custom elements must be closed (not self-enclosing), hence use **`<mycomp-test></mycomp-test>`** and not `<mycomp-test/>`
* In VS Code with html template literals define css-style in a string variable since the syntax highlighting will break otherwise.
* Don't forget binding eventhandlers "this" to class scope, eg: `this.onMyBtnClicked = this.onMyBtnClicked.bind(this);`
### Suggested improvements
* Make DebugWindow list multiple rows (info, warning and errors)

## Final words
This project does not represent a recommendation for enterprise development but rather what is already in the browsers today.
Be curious, use the right tool for the job. 
