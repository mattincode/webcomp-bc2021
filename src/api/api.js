/**
 * @file Base functionality for all api server calls.
 * Most basic actions can use this directly and will not need any extra api-classes.
 */
/* eslint-disable no-console */
const defaultFetchOptions = {
    //credentials: 'include',
    headers: {
        'Accept': 'application/json'
    }
};

const postOrPutFetchOptions = {
    //credentials: 'include',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    dataType: 'json'
};

/**
 * Http-get handler, is a plain function and will forward the handling of any errors to the caller.
 * @param api : (url)
 * @param isJson : Default true, set to false if manual parsing (for xml etc)
 * @returns {Promise} : Returns data as json.
 */
export function get(api, isJson = true) {
    return new Promise((resolve, reject) => {
        fetch(`${api}`, { ...defaultFetchOptions, method: 'get' })
            .then((response) => {
                if (response.ok) {
                    response.text().then(txt => {
                        let objectData = isJson ? JSON.parse(txt) : txt;
                        resolve(objectData);
                    });
                    //resolve(response.json());
                } else { // Internal server errors
                    response.text().then(errObj => {
                        console.error(errObj);
                        reject(new ApiError(ApiError.SERVER_ERROR, api, errObj));
                    });
                }
            }, (error) => { // Network errors
                console.error(error);
                reject(new ApiError(ApiError.NETWORK_ERROR, api));
            });
    });
}

/**
 * Http-post handler, is a plain function and will forward the handling of any errors to the caller.
 * @param api : (url)
 * @param data : The data to post
 * @returns {Promise} : Returns data as json.
 */
export function post(api, data) {
    return new Promise((resolve, reject) => {
        fetch(`${api}`, { ...postOrPutFetchOptions, method: 'post', body: JSON.stringify(data) })
            .then((response) => {
                if (response.ok) {
                    response.text().then(txt => {
                        let objectData = JSON.parse(txt);
                        resolve(objectData);
                    });
                } else {
                    response.text().then(errObj => {
                        console.error(errObj);
                        reject(new ApiError(ApiError.SERVER_ERROR, api, errObj));
                    });
                }
            }, (error) => {
                console.error(error);
                reject(new ApiError(ApiError.NETWORK_ERROR, api));
            });
    });
}

export const NETWORK_ERROR = 'NETWORK_ERROR';
export const SERVER_ERROR = 'SERVER_ERROR';

export class ApiError {
    constructor(errorType, apiCall, errorObjectAsJSON) {
        this.errorType = errorType;
        this.apiCall = apiCall;
        if (errorObjectAsJSON) {
            let errObj = JSON.parseWithDate(errorObjectAsJSON);
            this.exceptionMessage = errObj.exceptionMessage;
        } else {
            this.exceptionMessage = "{no message}";
        }
        if (this.errorType === NETWORK_ERROR) {
            this.title = "Network error"; 
            this.errorMessage = `API call "${this.apiCall}" failed due to network issues`; 
        } else {
            this.title = "Server error"; 
            this.errorMessage = `API call "${this.apiCall}" failed due to a server exception. Message: ${this.exceptionMessage}`; 
        }
    }
}