// https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements#using_the_lifecycle_callbacks
// customElements.define('word-count', WordCount, { extends: 'p' });
import { eventTypesClick } from './constants/eventTypes.js';
export class CheckboxControl extends HTMLElement {
  constructor() {
    super();
    this.onCheckboxClicked = this.onCheckboxClicked.bind(this);
    this._chkCtrl = null;
    this._status = null;
    this._storage = window.sessionStorage;
    this._root = this.attachShadow({ mode: "open" });
    const style = "<style>@import './global.css'</style>";
    this._root.innerHTML = /*html*/ `
    ${style}    
    <div>
      <input id="chk" type="checkbox"/>
      <label for="chk" id="status"></label>      
    </div>     
    `;
  }

  onCheckboxClicked(event) {
    this._status.innerHTML = event.currentTarget.checked ? "checked" : "unchecked";
    this._storage.setItem('chkState', this._chkCtrl.checked);
  }

  // Appended to a DOM-element
  connectedCallback() {
    // https://developer.mozilla.org/en-US/docs/Web/Events
    this._chkCtrl = this._root.querySelector('#chk');
    this._status = this._root.querySelector('#status');
    this._chkCtrl.checked = this._storage.getItem('chkState') === 'true' ? true : false;
    this._status.innerHTML = this._storage.getItem('chkState') === 'true' ? 'checked' : 'unchecked';
    this._chkCtrl.addEventListener(eventTypesClick.click, this.onCheckboxClicked);
  }

  // Removed from DOM
  disconnectedCallback() {
    this._chkCtrl.removeEventListener(eventTypesClick.click, this.onCheckboxClicked);
    this._storage.setItem('chkState', this._chkCtrl.checked);
  }

  // Invoked each time the custom element is moved to a new document.
  adoptedCallback() {

  }

  // Attributes changed
  // Note: Need to observer using: static get observedAttributes() { return ['c', 'l']; }
  attributeChangedCallback(name, oldValue, newValue) {

  }
}

customElements.define("checkbox-control", CheckboxControl);