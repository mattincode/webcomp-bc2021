/* eslint-disable no-console */
import { LayoutOrientation, LayoutItemOptions } from "./LayoutControl.js";
import "./CheckboxControl.js";
import "./DebugWindow.js";
import "./NewsControl.js";

const DBName = 'myDB';

class App extends HTMLElement {  
  constructor() {
    super();
    window.onerror = this.onApplicationError; // Catch application level errors
    this.onApplicationError = this.onApplicationError.bind(this);
    this.onDbError = this.onDbError.bind(this);
    this.onDbSuccess = this.onDbSuccess.bind(this);
    this.onDbUpgrade = this.onDbUpgrade.bind(this);
    this.onAddData = this.onAddData.bind(this);    
    this._db = null;           
    this._dbInfo = null;
    this._root = this.attachShadow({ mode: "open" });    
    const layoutOptions = [new LayoutItemOptions({ index: 1, fillSizeRelative: 1 }), new LayoutItemOptions({ index: 2, fillSizeRelative: 0 })];
    const style = "<style>@import './global.css'</style>";    
    this._root.innerHTML = /*html*/ `
    ${style}
    <debug-window></debug-window>
    <header class="w3-container">
      <h1>Web Components</h1>
    </header>
    <hr class="rounded">
    <h3>Custom control using properties</h3>
    <section class="w3-container w3-pale-green" style="height: 100%">      
      <layout-control usePadding=true id="oneX" orientation=${LayoutOrientation.Vertical} itemOptions=${JSON.stringify(layoutOptions)}>
        <div style="background: lightGrey; height: 100%">One</div>
        <div style="background: cyan; height: 100%">Two</div>
      </layout-control>          
    </section>
    <hr/>
    <h3>Checkbox control using session storage</h3>
    <section>
      <checkbox-control></checkbox-control>
    </section>
    <hr/>
    <h3>Local SqLite storage</h3>
    <section>
      <div id="dbInfo"></div>        
      <button class="w3-button w3-black" id="AddDataBtn" type="button">Add data</button>
    </section>
    <hr/>
    <h3>Custom component using api</h3>
    <section>
      <news-control></news-control>
    </section>
  `;
  }

  onApplicationError(msg, url, lineNo, columnNo, error) {
    console.error(msg, url, lineNo, columnNo, error);
  }

  onAddData() {
    const transaction = this._db.transaction(["toDoList"], "readwrite");
    /* report on the success of the transaction completing, when everything is done */
    transaction.oncomplete = () => {
      this._dbInfo.innerHTML = 'Transaction completed: database modification finished';
      /* TODO: update the display of data to show the newly added item */
    };

    transaction.onerror = () => {
      this._dbInfo = 'Transaction not opened due to error: ' + transaction.error;
    };

    const newItem = [
      { taskTitle: Date().toString(), hours: 12, minutes: 20, day: 1, month: 1, year: 2012, notified: "no" }
    ];

    /* Make a request to add our newItem object to the object store */
    const objectStore = transaction.objectStore("toDoList");
    const objectStoreRequest = objectStore.add(newItem[0]);
    objectStoreRequest.onsuccess = (event) => {
      this._dbInfo.innerHTML = 'Data added';
      /* report the success of our request
      (to detect whether it has been succesfully
      added to the database, you'd look at transaction.oncomplete) */
    };

  }

  onDbError(event) {
    this._dbInfo.innerHTML = 'Error loading database';
  }

  onDbSuccess(event) {
    this._dbInfo.innerHTML = 'Database initialised';
    this._db = event.currentTarget.result;
  }

  onDbUpgrade(event) {
    this._dbInfo.innerHTML = 'Database upgrade needed';
    const db = event.currentTarget.result; /* IDBDatabase */
    if (!db.objectStoreNames.contains("toDoList")) {
      const objectStore = db.createObjectStore("toDoList", { keyPath: "taskTitle" });
      // define what data items the objectStore will contain    
      objectStore.createIndex("hours", "hours", { unique: false });
      objectStore.createIndex("minutes", "minutes", { unique: false });
      objectStore.createIndex("day", "day", { unique: false });
      objectStore.createIndex("month", "month", { unique: false });
      objectStore.createIndex("year", "year", { unique: false });
      objectStore.createIndex("notified", "notified", { unique: false });
    }
  }

  connectedCallback() {
    console.log(`App reloaded: ${new Date().toLocaleTimeString()}`);    
    //console.warn(`No warnings`);
    //console.error(`No errors`);
    this._addDataBtn = this._root.querySelector("#AddDataBtn");
    this._addDataBtn.addEventListener('click', this.onAddData);
    this._dbInfo = this._root.querySelector("#dbInfo");
    //window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    // Let us open our database
    const ourDbVersion = 1; /* Increment to initiate an upgrade */
    window.indexedDB.deleteDatabase(DBName); /* For now just delete it all the time! */
    const DBOpenRequest = window.indexedDB.open(DBName, ourDbVersion);

    // these two event handlers act on the database being opened successfully, or not
    DBOpenRequest.onerror = this.onDbError;
    DBOpenRequest.onsuccess = this.onDbSuccess;
    DBOpenRequest.onupgradeneeded = this.onDbUpgrade;
  }
}

customElements.define("bootcamp-app", App);
