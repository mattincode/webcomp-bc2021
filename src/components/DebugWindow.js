/* eslint-disable no-console */
import { LayoutOrientation, LayoutItemOptions } from "./LayoutControl.js";

class DebugWindow extends HTMLElement {
  constructor() {
    super();
    this.onHeaderMouseDown = this.onHeaderMouseDown.bind(this);
    this.onHeaderMouseMove = this.onHeaderMouseMove.bind(this);
    this.onHeaderMouseUp = this.onHeaderMouseUp.bind(this);
    this.onConsoleLogOutput = this.onConsoleLogOutput.bind(this);
    this.onConsoleInfoOutput = this.onConsoleInfoOutput.bind(this);
    this.onConsoleWarnOutput = this.onConsoleWarnOutput.bind(this);
    this.onConsoleErrorOutput = this.onConsoleErrorOutput.bind(this);
    console.log = this.onConsoleLogOutput;
    console.info = this.onConsoleInfoOutput;
    console.warn = this.onConsoleWarnOutput;
    console.error = this.onConsoleErrorOutput;
    this._isMoving = false;
    this._mouseDownPosX = 0;
    this._mouseDownPosY = 0;
    let style = "<style>@import './components/DebugWindow.css'</style>";
    this._root = this.attachShadow({ mode: 'open' });
    this._root.innerHTML = /*html*/`
      ${style}
      <div id="Dialog" class="floating">
        <div id="Header" class="header">Debug window</div>
        <layout-control usePadding=true orientation=${LayoutOrientation.Vertical}>
          <div id="Log" class="log-content"></div>
          <div id="Info" class="info-content"></div>
          <div id="Warn" class="warn-content"></div>
          <div id="Error" class="error-content"></div>
        </layout-control>
      </div>      
    `;

  }

  connectedCallback() {
    this._headerElement = this._root.querySelector("#Header");
    this._dialogElement = this._root.querySelector("#Dialog");
    this._headerElement.addEventListener('mousedown', this.onHeaderMouseDown);
    this._logElement = this._root.querySelector("#Log");
    this._infoElement = this._root.querySelector("#Info");
    this._warnElement = this._root.querySelector("#Warn");
    this._errorElement = this._root.querySelector("#Error");
    document.addEventListener('mousemove', this.onHeaderMouseMove);
    document.addEventListener("mouseup", this.onHeaderMouseUp);
  }

  onConsoleLogOutput(data) {
    this._logElement.innerHTML = data;
    //window.log(data);
  }

  onConsoleInfoOutput(data) {
    this._infoElement.innerHTML = data;
  }

  onConsoleWarnOutput(data) {
    this._warnElement.innerHTML = data;
  }

  onConsoleErrorOutput(...data) {
    //TODO: Divide into formatted message!
    this._errorElement.innerHTML = data;
    //window.Error(data);
  }

  onHeaderMouseDown() {
    //throw ("ouch");
    this._isMoving = true;
    this._mouseDownPosX = this._dialogElement.offsetLeft;
    this._mouseDownPosY = this._dialogElement.offsetTop;
  }

  onHeaderMouseMove(mouseEvent) {
    if (this._isMoving) {
      let newPosX = this._mouseDownPosX + mouseEvent.movementX;
      let newPosY = this._mouseDownPosY + mouseEvent.movementY;
      this._dialogElement.style.left = `${newPosX}px`;
      this._dialogElement.style.top = `${newPosY}px`;
      this._mouseDownPosX = newPosX;
      this._mouseDownPosY = newPosY;
    }
  }

  onHeaderMouseUp() {
    this._isMoving = false;
  }
}

customElements.define('debug-window', DebugWindow);