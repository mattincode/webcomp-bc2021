// @flow
import {wrap} from "./lib/dom-extensions.js";

export const LayoutOrientation = {
  Horizontal: 0, // flexDirection: row
  Vertical: 1, // flexDirection: column
};

export const SimpleLayoutOption = {
  None: 0,
  FillLastItem: 1,
  StretchAll:2,
};

export class LayoutItemOptions {
  /*flow-include
    index: number;
    fillSizeRelative: number;
    alignEnd: ?boolean;   
  */
  constructor(init /*:LayoutItemOptions*/) {
    Object.assign(this, init);
  }
}

class LayoutControl extends HTMLElement {
  /*flow-include
    _root: ShadowRoot;    
    _id: string;    
    _usePadding: boolean;   
    _orientation: number;     
    _simpleOption: number; 
    _itemOptions: LayoutItemOptions[];
  */ 
  constructor() {    
    super();        
    this._root = this.attachShadow({ mode: "open" });          
    this._id = this.getAttribute("id") ? String(this.getAttribute("id")) : "container";      
    this._usePadding = Boolean(this.getAttribute("usePadding"));      
    this._orientation = this.getAttribute("orientation") ? parseInt(this.getAttribute("orientation"), 10) : LayoutOrientation.Horizontal; 
    this._simpleOption = this.getAttribute("simpleOption") ? parseInt(this.getAttribute("simpleOption"), 10) : SimpleLayoutOption.None; 
    // $FlowFixMe - This is typechecked but not from a flow perspective    
    this._itemOptions = this.getAttribute("itemOptions") ? JSON.parse(this.getAttribute("itemOptions")) : [];
    /* Initial render */    
    this._root.innerHTML = /*html*/ `      
      <div id=${this._id} class=${this._usePadding ? "container-with-padding" : "container"} style=${this._orientation === LayoutOrientation.Vertical ? "flex-direction:column" : "flex-direction:row"}>                  
        <slot>{/* The children will be rendered inside the slot */}</slot>              
      </div>      
      <style>@import './components/LayoutControl.css'</style>   
    `;
  }
  
  connectedCallback() {
    const container = this._root.querySelector(`#${this._id}`); 
    if (this._simpleOption !== SimpleLayoutOption.None && this._itemOptions.length > 0) {
      /* $FlowFixMe */
      container.innerHTML = /*html*/ `<div style="color: red">LayoutControl error - can't set both simpleOption and itemsOptions, pick one!</div>`;        
    }        
    if (container.childElementCount === 0) {      
      container.innerHTML = /*html*/ `<div/>`;              
    }            
    const children = container.children[0].assignedElements();       
    let index = 0;
    const count = children.length;
    for (const child of children) {
      const wrapper = document.createElement("div");
      const options = this.getLayoutOptions(index);
      const justifyContent = options && options.alignEnd ? "flex-end" : null;
      const isStretchAll = this._simpleOption === SimpleLayoutOption.StretchAll;
      const isFillLast = this._simpleOption === SimpleLayoutOption.FillLastItem;
      let relativeSize = isStretchAll ? 1 : options ? options.fillSizeRelative : null;

      /* Set margins on all children except the last one */      
      if (count === index + 1) {
        relativeSize = this._simpleOption === SimpleLayoutOption.FillLastItem ? 1 : relativeSize;        
        wrapper.setAttribute("style", `flex-grow:${relativeSize}; flex:${(isStretchAll || isFillLast) ? 1 : relativeSize}; align-self:${justifyContent}`);        
      } else if (this._orientation === LayoutOrientation.Horizontal) {        
        wrapper.setAttribute("style", `flex-grow:${relativeSize}; flex:${isStretchAll ? 1 : relativeSize}; align-self:${justifyContent}`);   
        wrapper.setAttribute("class", `horizontalChild`);
      } else {        
        wrapper.setAttribute("style", `flex-grow:${relativeSize}; flex:${isStretchAll ? 1 : relativeSize}; align-self:${justifyContent}`);   
        wrapper.setAttribute("class", `verticalChild`);
      }
      wrap(child, wrapper);      
      index += 1;
    }
    // If we had named slots we could easily wrap each element with appro
  }

  getLayoutOptions(index /*:number*/) /*:?LayoutItemOptions*/ {
    if (!this._itemOptions || this._itemOptions.length === 0) {
      return null;
    }
    const options = this._itemOptions.find(o => o.index === index + 1);
    return options;
  }
}

customElements.define("layout-control", LayoutControl);