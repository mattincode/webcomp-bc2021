
/* eslint-disable no-console */
import * as Api from "../api/api.js";
import { eventTypesClick } from "./constants/eventTypes.js";
import { LayoutOrientation, SimpleLayoutOption } from "./LayoutControl.js";

class NewsControl extends HTMLElement {
  constructor() {
    super();
    this.onRefreshBtnClicked = this.onRefreshBtnClicked.bind(this);
    this.getNews = this.getNews.bind(this);
    this._news = [];
    this._root = this.attachShadow({ mode: "open" });    
    const style = "<style>@import './global.css'; @import './components/NewsControl.css'</style>";
    this._root.innerHTML = /*html*/ `
    ${style}
    <header class="w3-container w3-dark-grey">
      <layout-control orientation=${LayoutOrientation.Horizontal} simpleOption=${SimpleLayoutOption.FillLastItem}>
        <h2 class="w3-text-color-white">News</h2>
        <button id="RefreshBtn" type="button" class="w3-button w3-white">Refresh</button>
      </layout-control>
    </header>    
    <section class="w3-container w3-border">
      <ul id="NewsList" class="w3-ul"></ul>
    </section>   
  `;
  }

  connectedCallback() {
    this._newsList = this._root.querySelector("#NewsList");
    this._refreshBtn = this._root.querySelector("#RefreshBtn");
    this._refreshBtn.addEventListener(eventTypesClick.click, this.onRefreshBtnClicked);    
    this.getNews();
  }

  onRefreshBtnClicked() {    
    this.getNews();
  }

  getNews() {
    console.log(`Loading news ${new Date().toLocaleTimeString()}`);
    Api.get("https://rss.aftonbladet.se/rss2/small/pages/sections/nyheter/", false)      
      .then(str => {
        const data = new window.DOMParser().parseFromString(str, "text/xml");
        this._news = [];
        let items = data.querySelectorAll("item");
        items.forEach(item => {
          let title = item.querySelector("title").childNodes[0].data;
          this._news.push(title);
        });
        // TODO #1: Parse items with title, description, link, image and display in a sub-component (news-item)
        // TODO #2: Add like-button to sub-component... and save the liked state... hint use the guid as key
        // TODO #3: Save news in local storage and use the stored data to display initial data on load (also include "Last updated")
        this._newsList.innerHTML = "";
        this._news.forEach(newsItem => {
          let li = document.createElement("li");
          li.innerHTML = newsItem;
          this._newsList.appendChild(li);
        });
        console.log(`News loaded: ${new Date().toLocaleTimeString()}`);
      });
  }
}

customElements.define("news-control", NewsControl);