/* @flow */

/*::
class Root extends Document {
  store: Store;
}

type Store = {
  tabs: Array<Tab>;
};

type Tab = {
  name: string;
  state: any;
};

type StoreNodeEnum = 'tabs';

type StoreNode = {
  node: StoreNodeEnum;
}
*/
const storeNode = {
  Tabs: 'tabs',
};

function getStore() /* :Store */ {
  //$FlowFixMe
  const root /*:Root */ = document;  
  return root.store;
}

export function initStore() {  
  //$FlowFixMe
  const root /*:Root */ = document;  
  root.store = {
    tabs: [],
  };
}

export function saveToStore(node /*:StoreNode*/, name /*:string */, variable /*:any */) {
  const store = getStore();
  switch(node) {
    case storeNode.Tabs: {
      const tab = store.tabs.find(tab => tab.name === name);
      if (tab) {
        tab.state = variable;
      } // TODO: Error handling if node not found!     
    }
    break;    
  }
}

export function getFromStore(node /*:StoreNode*/, name /*:string */) { 
  const store = getStore();
  switch(node) {
    case storeNode.Tabs: {
      const tab = store.tabs.find(tab => tab.name === name);
      if (tab) {
        return tab.state;
      } else { // TODO: Error handling if node not found!   
        return "";
      }
    }    
  } 
}
