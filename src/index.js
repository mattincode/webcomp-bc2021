/* eslint-disable */
import * as storeUtils from './utils/storeUtils.js';

storeUtils.initStore();

/* Debug */
// Hot-module reloading client side support
function watchFileChange() {
  const req = new XMLHttpRequest();
  req.timeout = 60000; // Keep request open until we open a new one
  req.addEventListener("error", function() {
      setTimeout(watchFileChange, 500);
  });
  req.responseType = 'text';
  req.addEventListener("load", function() {
      if (req.response === '{continue:true}') return watchFileChange();
      if (req.status === 200) {
          // do something with data, or not
          location.href = location.href
      }
  });
  req.open("GET", "/$watch?" + ('' + Math.random()).slice(2));
  req.send();
}

if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname.indexOf('10.') === 0) {
    watchFileChange();
}